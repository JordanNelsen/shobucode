﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shobu2
{
    class Board
    {
        public int boardNumber { get; private set; }
        public int xsOnBoard { get; private set; }
        public int osOnBoard { get; private set; }
        public bool xsAndOsOnBoard {
            get
            {
                return xsOnBoard > 0 && osOnBoard > 0;
            }
        }

        public Dictionary<string, Square> boardSquares;
        public Board(int boardNumber)
        {
            this.boardNumber = boardNumber;
            boardSquares = new Dictionary<string, Square>()
            {
                { "11", new Square(1, 1, boardNumber) },
                { "21", new Square(2, 1, boardNumber) },
                { "31", new Square(3, 1, boardNumber) },
                { "41", new Square(4, 1, boardNumber) },
                { "12", new Square(1, 2, boardNumber) },
                { "22", new Square(2, 2, boardNumber) },
                { "32", new Square(3, 2, boardNumber) },
                { "42", new Square(4, 2, boardNumber) },
                { "13", new Square(1, 3, boardNumber) },
                { "23", new Square(2, 3, boardNumber) },
                { "33", new Square(3, 3, boardNumber) },
                { "43", new Square(4, 3, boardNumber) },
                { "14", new Square(1, 4, boardNumber) },
                { "24", new Square(2, 4, boardNumber) },
                { "34", new Square(3, 4, boardNumber) },
                { "44", new Square(4, 4, boardNumber) },
            };
            for (int i = 1; i < 5; i++)
            {
                boardSquares["1" + i].hasX = true;
                xsOnBoard++;
                boardSquares["4" + i].hasO = true;
                osOnBoard++;
            }
        }

        // Checks the given starting location for a piece
        // from the owning player
        public bool SquareHasPlayersPiece(Move moveToCheck)
        {
            if (moveToCheck.player.name == "X")
            {
                return boardSquares[moveToCheck.startSquare].hasX;
            }
            else
            {
                return boardSquares[moveToCheck.startSquare].hasO;
            }
        }

        // Runs through the rules of a legal move, failing the check
        // if any rule is broken
        public bool MoveIsLegal(Move moveToCheck)
        {
            // if illegal distance, if two pieces in path, if own piece in path
            // opponent's piece in path and passive, not straight line
            Square start = boardSquares[moveToCheck.startSquare];
            Square end = boardSquares[moveToCheck.endSquare];
            if (start.xCoordinate + start.yCoordinate.ToString() == end.xCoordinate + end.yCoordinate.ToString())
            {
                Console.WriteLine("You must choose to move to a different square than where the piece started.");
                return false;
            }

            int distanceTraveledOnX = Math.Abs(start.xCoordinate - end.xCoordinate);
            int distanceTraveledOnY = Math.Abs(start.yCoordinate - end.yCoordinate);
            Square middleSquare = end;
            bool hasPieceInSquarePastMove = false;

            // illegal distance
            if (((distanceTraveledOnX < 3) && (distanceTraveledOnY < 3)) == false)
            {
                Console.WriteLine("You may only move up to 2 squares away.");
                return false;
            }

            // not straight line
            if (start.xCoordinate != end.xCoordinate && start.yCoordinate != end.yCoordinate && distanceTraveledOnX != distanceTraveledOnY)
            {
                Console.WriteLine("You must move in a straight line.");
                return false;
            }

            // establish transition square if required
            bool movesTwoSpaces;
            if (distanceTraveledOnY < 2 && distanceTraveledOnX < 2)
            {
                movesTwoSpaces = false;
            } else if (distanceTraveledOnX == 2 && distanceTraveledOnY == 2)
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[(((start.yCoordinate + end.yCoordinate) / 2).ToString() + (start.xCoordinate + end.xCoordinate) / 2)];
            } else if (distanceTraveledOnY == 2)
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[((start.yCoordinate + end.yCoordinate) / 2) + start.xCoordinate.ToString()];
            } else
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[start.yCoordinate.ToString() + ((start.xCoordinate + end.xCoordinate) / 2)];
            }

            // two pieces in path EXCLUDING SQUAREPASTMOVE
            if (movesTwoSpaces)
            {
                if ((end.hasX || end.hasO) && (middleSquare.hasX || middleSquare.hasO)) {
                    Console.WriteLine("You may not push 2 other pieces with your move.");
                    return false;
                }
            }

            // own piece in path
            if ((moveToCheck.player.name == "X") && (end.hasX)) 
            {
                Console.WriteLine("You may never push your own pieces.");
                return false;
            } else if ((moveToCheck.player.name == "O") && (end.hasO))
            {
                Console.WriteLine("You may never push your own pieces.");
                return false;
            }
            if (movesTwoSpaces)
            {
                if ((moveToCheck.player.name == "X") && (middleSquare.hasX))
                {
                    Console.WriteLine("You may never push your own pieces.");
                    return false;
                } else if ((moveToCheck.player.name == "O") && (middleSquare.hasO))
                {
                    Console.WriteLine("You may never push your own pieces.");
                    return false;
                }
            }

            // find squarePastMove
            int squarePastMoveX = 0;
            if (distanceTraveledOnX == 0)
            {
                squarePastMoveX = start.xCoordinate;
            } else if (start.xCoordinate > end.xCoordinate)
            {
                squarePastMoveX = end.xCoordinate - 1;
            } else if (start.xCoordinate < end.xCoordinate)
            {
                squarePastMoveX = end.xCoordinate + 1;
            }
            int squarePastMoveY = 0;
            if (distanceTraveledOnY == 0)
            {
                squarePastMoveY = start.yCoordinate;
            }
            else if (start.yCoordinate > end.yCoordinate)
            {
                squarePastMoveY = end.yCoordinate - 1;
            }
            else if (start.yCoordinate < end.yCoordinate)
            {
                squarePastMoveY = end.yCoordinate + 1;
            }

            // checks if move reaches end of board
            bool movesToEdgeOfBoard = true;
            Square squarePastMove = new Square(0, 0, 5);
            if ((squarePastMoveX > 0 && squarePastMoveX < 5) && (squarePastMoveY > 0 && squarePastMoveY < 5))
            {
                squarePastMove = boardSquares[squarePastMoveX.ToString() + squarePastMoveY.ToString()];
                movesToEdgeOfBoard = false;
            }

            // piece in squarePastMove
            if (!movesToEdgeOfBoard)
            {
                if (squarePastMove.hasO || squarePastMove.hasX)
                {
                    hasPieceInSquarePastMove = true;
                }
            }

            // check for 2 pieces in path INCLUDING SQUAREPASTMOVE
            if (distanceTraveledOnX == 2 || distanceTraveledOnY == 2)
            {
                if ((middleSquare.hasO || middleSquare.hasX) && hasPieceInSquarePastMove) {
                    Console.WriteLine("You may not push 2 other pieces with one move.");
                    return false;
                }
            }
            if ((end.hasX || end.hasO) && hasPieceInSquarePastMove)
            {
                Console.WriteLine("You may not push 2 other pieces with one move.");
                return false;
            }

            // check for piece in path while passive
            if (moveToCheck.isPassive)
            {
                if (middleSquare.hasO || middleSquare.hasX)
                {
                    Console.WriteLine("You may not push pieces during your passive move.");
                    return false;
                }
                else if (end.hasX || end.hasO)
                {
                    Console.WriteLine("You may not push pieces during your passive move.");
                    return false;
                }
            }
            return true;
        }

        public void ExecuteMove(Move move)
        {
            // move piece on end square, move piece on mid square if applicable
            // move original piece, track x/o counts throughout

            Square start = boardSquares[move.startSquare];
            Square end = boardSquares[move.endSquare];
            Square middleSquare = end;
            int distanceTraveledOnX = Math.Abs(start.xCoordinate - end.xCoordinate);
            int distanceTraveledOnY = Math.Abs(start.yCoordinate - end.yCoordinate);

            // find squarePastMove
            int squarePastMoveX = 0;
            if (distanceTraveledOnX == 0)
            {
                squarePastMoveX = start.xCoordinate;
            }
            else if (start.xCoordinate > end.xCoordinate)
            {
                squarePastMoveX = end.xCoordinate - 1;
            }
            else if (start.xCoordinate < end.xCoordinate)
            {
                squarePastMoveX = end.xCoordinate + 1;
            }
            int squarePastMoveY = 0;
            if (distanceTraveledOnY == 0)
            {
                squarePastMoveY = start.yCoordinate;
            }
            else if (start.yCoordinate > end.yCoordinate)
            {
                squarePastMoveY = end.yCoordinate - 1;
            }
            else if (start.yCoordinate < end.yCoordinate)
            {
                squarePastMoveY = end.yCoordinate + 1;
            }

            // checks if move reaches end of board
            bool movesToEdgeOfBoard = true;
            Square squarePastMove = new Square(0, 0, 5);
            if ((squarePastMoveX > 0 && squarePastMoveX < 5) && (squarePastMoveY > 0 && squarePastMoveY < 5))
            {
                squarePastMove = boardSquares[squarePastMoveX.ToString() + squarePastMoveY.ToString()];
                movesToEdgeOfBoard = false;
            }

            // establish transition square if required
            bool movesTwoSpaces;
            if (distanceTraveledOnY < 2 && distanceTraveledOnX < 2)
            {
                movesTwoSpaces = false;
            }
            else if (distanceTraveledOnX == 2 && distanceTraveledOnY == 2)
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[((start.xCoordinate + end.xCoordinate) / 2) + ((start.yCoordinate + end.yCoordinate) / 2).ToString()];
            }
            else if (distanceTraveledOnY == 2)
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[start.xCoordinate.ToString() + ((start.yCoordinate + end.yCoordinate) / 2)];
            }
            else
            {
                movesTwoSpaces = true;
                middleSquare = boardSquares[start.yCoordinate.ToString() + ((start.xCoordinate + end.xCoordinate) / 2)];
            }

            // move piece on end square
            if (end.hasO)
            {
                end.hasO = false;
                if (movesToEdgeOfBoard)
                {
                    osOnBoard--;
                } else { squarePastMove.hasO = true; }
            }
            if (end.hasX)
            {
                end.hasX = false;
                if (movesToEdgeOfBoard)
                {
                    xsOnBoard--;
                }
                else { squarePastMove.hasX = true; }
            }

            // move piece on middle square if required
            if (movesTwoSpaces)
            {
                if (middleSquare.hasO)
                {
                    middleSquare.hasO = false;
                    if (movesToEdgeOfBoard)
                    {
                        osOnBoard--;
                    }
                    else { squarePastMove.hasO = true; }
                }
                if (middleSquare.hasX)
                {
                    middleSquare.hasX = false;
                    if (movesToEdgeOfBoard)
                    {
                        xsOnBoard--;
                    }
                    else { squarePastMove.hasX = true; }
                }
            }

            // move original piece
            if (start.hasX)
            {
                start.hasX = false;
                end.hasX = true;
            } else
            {
                start.hasO = false;
                end.hasO = true;
            }
        }

        // Runs through a row of the board, returning the appropriate
        // symbol for player's pieces or blank squares
        public string GetRowAsString(int row)
        {
            string result = "|";
            for (int i = 1; i < 5; i ++)
            {
                if (boardSquares[row + i.ToString()].hasX)
                {
                    result += "X";
                } else if (boardSquares[row + i.ToString()].hasO)
                {
                    result += "O";
                } else
                {
                    result += "-";
                }
                result += "|";
            }
            return result;
        }
    }
}
