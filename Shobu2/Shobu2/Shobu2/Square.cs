﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shobu2
{
    // A set of x/y coordinates, whether there is a piece in the square,
    // and which board it belongs to
    class Square
    {
        public int xCoordinate { get; private set; }
        public int yCoordinate { get; private set; }
        public bool hasX;
        public bool hasO;
        public int board { get; private set; }

        public Square(int yCoordinate, int xCoordinate, int board)
        {
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
            this.board = board;
            hasX = false;
            hasO = false;
        }
    }
}
