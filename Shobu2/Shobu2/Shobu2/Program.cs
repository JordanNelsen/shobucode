﻿using System;
using System.Collections.Generic;

namespace Shobu2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates the 4 boards the game uses
            Board board1 = new Board(1);
            Board board2 = new Board(2);
            Board board3 = new Board(3);
            Board board4 = new Board(4);
            Board[] allBoards = new Board[4]
            {
                board1, board2, board3, board4
            };

            // Creates the 2 players and sets the starting player
            Player playerX = new Player("X");
            Player playerO = new Player("O");
            Player currentPlayer = playerX;
            
            // Sets bools that track the end game condition and turn type
            bool gameContinues = true;
            bool passiveMove = true;

            // Introduces game and displays rules
            Console.WriteLine("Welcome to the game of Shobu!");
            Console.WriteLine();
            Console.WriteLine(Game.rules);
            Console.WriteLine();
            Console.WriteLine("Press enter to begin.");
            Console.ReadLine();

            // Loops from turn to turn until end of game
            while (gameContinues)
            {
                // If any checks fail, the current turn is reset

                // Refreshes the display
                Game.Refresh(allBoards);
                Console.WriteLine();

                // Establishes the current player's homeboards
                int home1 = currentPlayer.homeBoards[0];
                int home2 = currentPlayer.homeBoards[1];

                // Establishes if this is a passive or aggressive move and displays current player
                // and previous passive move if applicable
                string turnType = "passive";
                if (!passiveMove)
                {
                    turnType = "aggressive";
                    Console.WriteLine($"Last passive move was from {currentPlayer.getLastMoveAsString()} on board {currentPlayer.lastBoardChosen + 1}.");
                }
                Console.WriteLine($"Player {currentPlayer.name}'s turn, {turnType} move.");

                // Prompts the user for a board selection or a request to view the rules.
                Console.WriteLine($"Player {currentPlayer.name}, select a board to move on (1-4) or type \"rules\":");
                string selectedBoardInput = Console.ReadLine();
                switch (selectedBoardInput)
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                        break;
                    case "rules":
                        Console.Clear();
                        Console.WriteLine(Game.rules);
                        Console.WriteLine("\nPress enter to resume.");
                        Console.ReadLine();
                        continue;
                    default:
                        Console.WriteLine("Not a valid board. Press enter to continue.");
                        Console.ReadLine();
                        continue;
                }
                int selectedBoard = int.Parse(selectedBoardInput) - 1;
                if ((selectedBoard > 3 || selectedBoard < 0))
                {
                    Console.WriteLine("Not a valid board. Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }

                // Checks that selected board is a home board if the current turn is passive.
                if (passiveMove && (selectedBoard + 1 != home1 && selectedBoard + 1 != home2))
                {
                    Console.WriteLine("Passive moves must be made on a home board. Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }

                // Checks that selected board is of a different color than the passive move
                // if the current move is aggressive.
                if (!passiveMove)
                {
                    bool lastBoardWasOdd = (currentPlayer.lastBoardChosen % 2 == 1);
                    bool selectedBoardIsOdd = (selectedBoard % 2 == 1);
                    if (lastBoardWasOdd == selectedBoardIsOdd)
                    {
                        Console.WriteLine("Aggressive moves must be made on board of other color. Press enter to continue.");
                        Console.ReadLine();
                        continue;
                    }
                }

                // Prompts the user to select a piece and generates a partial "Move" based on
                // the selection.
                Console.WriteLine($"Player {currentPlayer.name}, select a piece to move (e.g. \"a4\") :");
                string input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Not a valid square.  Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }
                if (input.Length < 2)
                {
                    input = "fail";
                }
                string selectedPiece = Game.convertLetterToNum(input);
                Move testMove = new Move(selectedPiece, "bogus", currentPlayer, false);
                
                // Checks if selected square is a real square
                if (!allBoards[selectedBoard].boardSquares.ContainsKey(testMove.startSquare))
                {
                    Console.WriteLine("Not a valid square.  Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }

                // Checks that selected has a piece owned by current player
                if (!allBoards[selectedBoard].SquareHasPlayersPiece(testMove))
                {
                    Console.WriteLine("You don't have a piece there.  Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }

                // Prompts the user to select a destination for their piece and
                // generates a "Move"
                Console.WriteLine("Select a square to move to:");
                input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Not a valid square.  Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }
                if (input.Length < 2)
                {
                    input = "fail";
                }
                string selectedSquare = Game.convertLetterToNum(input);
                Move fullMove = new Move(selectedPiece, selectedSquare, currentPlayer, passiveMove);

                // Checks that selected destination is an existing square
                if (!allBoards[selectedBoard].boardSquares.ContainsKey(fullMove.endSquare))
                {
                    Console.WriteLine("Not a valid square.  Press enter to continue");
                    Console.ReadLine();
                    continue;
                }

                // If current move is aggressive, checks that it is the same distance
                // and direction as the passive move
                bool passiveAndAggroMatch;
                if (!passiveMove)
                {
                    string[] lastMove = currentPlayer.lastMoveChosen;
                    string[] currentMove = new string[2] { fullMove.startSquare, fullMove.endSquare };
                    passiveAndAggroMatch = Game.AggressiveMoveMatchesPassive(lastMove, currentMove);
                    if (!passiveAndAggroMatch)
                    {
                        Console.WriteLine("Aggressive move must be the same distance and direction as passive move.");
                        Console.WriteLine("Press enter to continue.");
                        Console.ReadLine();
                        continue;
                    }
                }

                // Asks board to check that the proposed move is legal. If
                // it is, it is executed and the turn advances.
                if (allBoards[selectedBoard].MoveIsLegal(fullMove))
                {
                    allBoards[selectedBoard].ExecuteMove(fullMove);
                }
                else
                {
                    Console.WriteLine("Press enter to continue.");
                    Console.ReadLine();
                    continue;
                }


                // Game.Refresh(allBoards);
                if (passiveMove)
                {
                    currentPlayer.lastBoardChosen = selectedBoard;
                    currentPlayer.lastMoveChosen[0] = fullMove.startSquare;
                    currentPlayer.lastMoveChosen[1] = fullMove.endSquare;
                    passiveMove = false;
                } else
                {
                    currentPlayer = currentPlayer.name == "X" ? playerO : playerX;
                    passiveMove = true;
                }
                if (Game.ABoardHasOnlyXsOrOs(allBoards))
                {
                    gameContinues = false;
                }
            }

            Game.Refresh(allBoards);
            bool gameConcluded = false;
            Console.WriteLine();

            // Checks all 4 boards for the win condition: a player has
            // removed all their opponent's pieces from 1 board
            foreach(Board board in allBoards)
            {
                if (board.xsAndOsOnBoard == false)
                {
                    if (board.xsOnBoard < 1)
                    {
                        Console.WriteLine($"Player O is the winner!");
                        gameConcluded = true;
                    } else if (board.osOnBoard < 1)
                    {
                        Console.WriteLine("Player X is the winner!");
                        gameConcluded = true;
                    }
                }
            }
            if (gameConcluded == false)
            {
                Console.WriteLine("Sorry, something went wrong!  Press enter to exit.");
            }
            Console.WriteLine();
            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }
    }
}

// TO DO: MAKE MORE INPUT CHECKS