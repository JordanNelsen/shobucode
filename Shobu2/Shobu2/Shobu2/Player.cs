﻿namespace Shobu2
{
    public class Player
    {
        // Contains the player's home boards, last passive move,
        // and player name
        public string name { get; private set; }
        public int[] homeBoards { get; private set; }
        public int lastBoardChosen;
        public string[] lastMoveChosen;
        
        // Returns the passive move as a letter+number combo,
        // both start and end squares
        public string getLastMoveAsString()
        {
            string result = "";

            foreach (string square in lastMoveChosen)
            {
                    switch (square[0])
                    {
                        case '1':
                            result += 'a';
                            break;
                        case '2':
                            result += 'b';
                            break;
                        case '3':
                            result += 'c';
                            break;
                        case '4':
                            result += 'd';
                            break;
                        default:
                            result += square[0];
                            break;
                    }
                result += square[1] + " to ";
            }
            return result.Substring(0, 8);
        }

        public Player(string name)
        {
            this.name = name;
            lastMoveChosen = new string[2];
            homeBoards = new int[2];
            if (name == "X")
            {
                homeBoards[0] = 1;
                homeBoards[1] = 2;
            } else
            {
                homeBoards[0] = 3;
                homeBoards[1] = 4;
            }
        }
    }
}