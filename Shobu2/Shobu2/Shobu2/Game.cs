﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shobu2
{
    class Game
    {
        // Updates the console based on current position of pieces.
        static public void Refresh(Board[] boards)
        {
            Board board1 = boards[0];
            Board board2 = boards[1];
            Board board3 = boards[2];
            Board board4 = boards[3];
            string[] letterList = new string[4] { "A", "B", "C", "D" };

            Console.Clear();
            Console.WriteLine("                   Player X");
            Console.WriteLine("              1 2 3 4   1 2 3 4");
            for (int i = 1; i < 5; i++)
            {
                if (i == 2)
                {
                    Console.Write("Board 1   ");
                }else
                {
                    Console.Write("          ");
                }
                Console.Write($" {letterList[i - 1]} ");
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(board1.GetRowAsString(i));
                Console.ResetColor();
                Console.Write(" ");
                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(board2.GetRowAsString(i));
                Console.ResetColor();
                Console.Write($" {letterList[i - 1]}");
                if (i == 2)
                {
                    Console.WriteLine("    Board 2");
                } else
                {
                    Console.WriteLine("          ");
                }
            }
            Console.WriteLine();
            for (int i = 1; i < 5; i++)
            {
                if (i == 3)
                {
                    Console.Write("Board 3   ");
                }
                else
                {
                    Console.Write("          ");
                }
                Console.Write($" {letterList[i - 1]} ");
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(board3.GetRowAsString(i));
                Console.ResetColor();
                Console.Write(" ");
                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(board4.GetRowAsString(i));
                Console.ResetColor();
                Console.Write($" {letterList[i - 1]}");
                if (i == 3)
                {
                    Console.WriteLine("    Board 4");
                }
                else
                {
                    Console.WriteLine("          ");
                }
            }
            Console.WriteLine("              1 2 3 4   1 2 3 4");
            Console.WriteLine("                   Player O");
        }

        // Takes a letter+number combo and converts it to a number+number combo
        static public string convertLetterToNum(string squareAsLetterNum)
        {
            string squareAsNumNum = "";
            int firstPart = squareAsLetterNum.ToLower()[0];
            firstPart -= 96;
            squareAsNumNum += firstPart.ToString() + squareAsLetterNum[1];
            return squareAsNumNum;
        }

        // Checks the passive move and the aggressive move are the same distance
        // and direction
        static public bool AggressiveMoveMatchesPassive(string[] passiveMove, string[] aggroMove)
        {
            string firstMoveStart = passiveMove[0];
            string firstMoveEnd = passiveMove[1];
            string secondMoveStart = aggroMove[0];
            string secondMoveEnd = aggroMove[1];
            int firstStartX = int.Parse(firstMoveStart[1].ToString());
            int firstStartY = int.Parse(firstMoveStart[0].ToString());
            int firstEndX = int.Parse(firstMoveEnd[1].ToString());
            int firstEndY = int.Parse(firstMoveEnd[0].ToString());
            int secondStartX = int.Parse(secondMoveStart[1].ToString());
            int secondStartY = int.Parse(secondMoveStart[0].ToString());
            int secondEndX = int.Parse(secondMoveEnd[1].ToString());
            int secondEndY = int.Parse(secondMoveEnd[0].ToString());

            if (firstStartX - firstEndX == secondStartX - secondEndX)
            {
                if (firstStartY - firstEndY == secondStartY - secondEndY)
                {
                    return true;
                }
            }
            return false;
        }

        // Checks to see if a board has only one player's pieces
        static public bool ABoardHasOnlyXsOrOs(Board[] allBoards)
        {
            foreach (Board board in allBoards)
            {
                if (board.xsAndOsOnBoard == false)
                {
                    return true;
                }
            }
            return false;
        }

        static public string rules =
            "The goal of Shobu is to remove all of your opponents pieces on any ONE board.\n" +
            "On your turn, you make first a \"passive\" move, then an \"aggressive\" move.  The main\n" +
            "difference is that your passive move may not push any pieces, but the aggressive move may.\n\n" +
            "To make your passive move, select one of your home boards.  That is, one of the 2 boards on\n" +
            "your side of the screen.  Then, select one of your pieces, and choose where you would like\n" +
            "to move it.  You may move it horizontally, vertically, or diagonally up to 2 squares away,\n" +
            "but you may not push any pieces if it is your passive move.\n\n" +
            "Then for your aggressive move, you must choose one of the two boards of a DIFFERENT color\n" +
            "than the one you made your passive move on, but it does NOT have to be one of your home\n" +
            "boards.  It DOES have to move the same distance and direction as your passive move did.\n" +
            "This time, you may push one of your opponents pieces!  You may not push any of your\n" +
            "pieces and you may not push 2 pieces with one move.  If you are able to push your opponent's\n" +
            "piece off the board, it is removed from the game.  If you remove all of your opponent's\n" +
            "pieces from one board, you win!\n\n" +
            "Finally, it is possible to run out of legal moves.  If that happens, you lose.\n\n" +
            "Enjoy!  And thanks for playing!";
    }
}
