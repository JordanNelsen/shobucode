﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shobu2
{
    class Move
    {
        // The locations of the start and end of a move,
        // plus the player that made the move, and whether
        // or not the move is passive
        public string startSquare { get; private set; }
        public string endSquare { get; private set; }
        public Player player { get; private set; }
        public bool isPassive { get; private set; }

        public Move(string startSquare, string endSquare, Player player, bool isPassive)
        {
            this.startSquare = startSquare;
            this.endSquare = endSquare;
            this.player = player;
            this.isPassive = isPassive;
        }
    }
}
